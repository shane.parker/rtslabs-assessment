
class Assessment {
  aboveBelow(numbers, value) {
    return numbers.reduce((acc, v) => {
      const { above = 0, below = 0 } = acc

      return {
        above: (v > value) ? above + 1 : above,
        below: (v < value) ? below + 1 : below
      }
    }, {})
  }

  stringRotation(string, n) {
    const len = string.length
    const buffer = new Int32Array(len)

    for ( let x = 0; x < len; x++ ) {
      const sh = ((x+n) % len)
      buffer[sh] = string.codePointAt(x)
    }

    /**
      NOTE: This conversion could be done with TextDecoder,
      but that seems to cause issues in the unit test when int32
      arrays are used.
      
      This code was written to handle all unicode code points.
    */
    return buffer.reduce((acc, c) => acc + String.fromCharCode(c), '')
  }
}

module.exports = Assessment
