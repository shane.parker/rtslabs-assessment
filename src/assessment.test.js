const Assessment = require('./assessment')

test('Verifies aboveBelow method', () => {
  const a = new Assessment()

  const result = a.aboveBelow([1, 5, 2, 1, 10],6)

  expect(result).toEqual({
    above: 1,
    below: 4
  })
})

test('Verifies stringRotation method', () => {
  const a = new Assessment()

  const result = a.stringRotation('MyString', 2)

  expect(result).toEqual('ngMyStri')
})
