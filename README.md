# rtslabs-assessment

This is an implementation of the RTS Labs code assessment. This implementation has been done
in Javascript for the Node.js environment. Included is an Assessment class (src/assessment.js)
that implements two methods: stringRotation and aboveBelow. Unit tests are also included.

## Getting started

Install packages by running `npm i`

## Running unit tests

The assessment can be tested by running `npm run test`
